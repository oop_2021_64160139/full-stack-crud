import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
  ) {}

  create(createUserDto: CreateUserDto) {
    return this.userRepository.save(createUserDto);
  }

  findAll() {
    return this.userRepository.find();
  }

  findOne(id: number) {
    return this.userRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    const product = await this.userRepository.findOneBy({ id });
    if (!product) {
      throw new NotFoundException();
    }
    const updateUsers = { ...product, ...updateUserDto };
    return this.userRepository.save(updateUsers);
  }

  async remove(id: number) {
    const deleteUser = await this.userRepository.findOneBy({ id });
    if (!deleteUser) {
      throw new NotFoundException();
    }
    return this.userRepository.softRemove(deleteUser);
  }
}
